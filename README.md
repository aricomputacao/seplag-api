# Visão geral

O projeto é uma aplicação back-end com objetivo de criar serviço em
Java [Spring Boot](https://projects.spring.io/spring-boot), [Spring Data](http://projects.spring.io/spring-data/) para
consumir os "Microsoft Security Updates"
disponíveis da API da microsoft e persistir todos os dados coletados no banco de
dados [PostgreSQL](https://www.postgresql.org/docs/12/index.html)

## Tecnologias

- [Spring Boot](https://projects.spring.io/spring-boot) é uma ferramenta que simplifica a configuração e execução de
  aplicações Java stand-alone, com conceitos de dependências “starters”, auto configuração e servlet container embutidos
  é proporcionado uma grande produtividade desde o start-up da aplicação até sua ida a produção.

- [Spring Data](http://projects.spring.io/spring-data/) é um framework que abstrai o acesso ao modelo de dados,
  independente a tecnologia de base de dados.

- [Lombok](https://projectlombok.org/) é uma biblioteca Java focada em produtividade e redução de código boilerplate
  que, por meio de anotações adicionadas ao nosso código, ensinamos o compilador (maven ou gradle) durante o processo de
  compilação a criar código Java.

- [Flyway](https://documentation.red-gate.com/fd)  é um framework que permite o versionamento do Banco de dados.

# Setup da aplicação (local)

## Pré-requisito

Antes de rodar a aplicação é preciso garantir que as seguintes dependências estejam corretamente instaladas:

```
Java 17
PostgreSQL 12
Maven 3.8.6 
```

## Funcionalidades

- Consultar os registros de atualizações.
- Remover registros de atualização.
- Rodar uma Timer/Schedule a cada 5 minutos inserindo atualizações. 



## Preparando ambiente

É necessário a criação da base de dados relacional no Postgres

```
CREATE DATABASE seplag;
```

## Instalação da aplicação

Primeiramente, faça o clone do repositório:

```
https://gitlab.com/aricomputacao/seplag-api.git
```

Feito isso, acesse o projeto:

```
cd seplag-api/api
```

É preciso compilar o código e baixar as dependências do projeto:

```
mvn clean package
```

Finalizado esse passo, vamos iniciar a aplicação:

```
 mvn spring-boot:run -Dspring-boot.run.arguments=--spring.datasource.username=USUARIO_POSTRGRES -Dspring-boot.run.arguments=--spring.datasource.password=SENHA```
```
Pronto. A aplicação está disponível em http://localhost:8080

Tomcat started on port(s): 8080 (http)
Started AppConfig in xxxx seconds (JVM running for xxxx)
```

# APIs

O projeto disponibiliza uma API que utiliza o padrão Rest de comunicação, produzindo e consumindo arquivos no formato
JSON.

Segue abaixo as APIs disponíveis no projeto:

#### Atualizacoes

- /api/v1/atualizacoes (GET)
    - Pode ser passado parâmetros (ID,Alias,DocumentTitle,InitialReleaseDate,CurrentReleaseDate,CvrfUrl) como filtro.
    - Retorna um pageable.
     ```
   
     {
			"ID": "2016-Jan",
			"Alias": "2016-Jan",
			"DocumentTitle": "January 2016 Security Updates",
			"Severity": null,
			"InitialReleaseDate": "2016-01-12T08:00:00Z",
			"CurrentReleaseDate": "2016-05-25T07:00:00Z",
			"CvrfUrl": "https://api.msrc.microsoft.com/cvrf/v2.0/document/2016-Jan"
	 }
   
   ```
- /api/v1/atualizacoes/consultar-ids (GET)
    - Retorna uma lista com os ID's registrados.
    ```
  {
   "descricao": "2016-Jan"		
  }
         
    ```
- /api/v1/atualizacoes/consultar-quantidade-ano (GET)
    - Retorna uma lista com o ano e a quantidade de registros de atualização.
     ```
   {
      "ano": 2016,
      "quantidade": 10		
   }
     ```
- /api/v1/atualizacoes/{ID} (DELETE)
  - Recebe no path o valor do ID que deve ser excluid.
