package org.seplag.api.commom;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Getter
@Setter
@Component
@ConfigurationProperties("api")
public class ApiProperty {


    private String origemPermitida = "http://localhost:800";



}
