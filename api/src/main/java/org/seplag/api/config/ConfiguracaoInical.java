package org.seplag.api.config;

import lombok.extern.slf4j.Slf4j;
import org.seplag.api.repository.AtualizacaoDeSegurancaRepository;
import org.seplag.api.service.AtualizacaoDeSegurancaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.util.List;
@Slf4j
@Configuration
public class ConfiguracaoInical {


    public ConfiguracaoInical(AtualizacaoDeSegurancaRepository atualizacaoDeSegurancaRepository, AtualizacaoDeSegurancaService atualizacaoDeSegurancaService) {
        this.atualizacaoDeSegurancaRepository = atualizacaoDeSegurancaRepository;
        this.atualizacaoDeSegurancaService = atualizacaoDeSegurancaService;
    }

    private AtualizacaoDeSegurancaRepository atualizacaoDeSegurancaRepository;
    private AtualizacaoDeSegurancaService atualizacaoDeSegurancaService;


    //Método executado antes da aplicação começar a aceitar requisições
    @EventListener(ContextRefreshedEvent.class)
    public void iniciando() {

        log.info("++++++ INCIANDO APLICAÇÃO");

        //EXECUTANDO ROTINA PARA POPULAR A TABELA QUANDO APLICAÇÃO ESTIVER INICIANDO
        List<String> listaDeIdsRegistrados = atualizacaoDeSegurancaRepository.consultarIds();
        atualizacaoDeSegurancaService.salvarAtualizacao();

        log.info("++++++ APLICAÇÃO INICIADA COM SUCESSO");


    }
}
