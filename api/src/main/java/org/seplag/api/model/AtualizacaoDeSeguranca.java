package org.seplag.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "atualizacao_seguranca")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AtualizacaoDeSeguranca {

    @Id
    @Column(name = "ats_id", updatable = false)
    @Setter(AccessLevel.NONE)
    @JsonProperty("ID")
    private String id;
    @JsonProperty("Alias")
    @NotBlank
    @Column(name = "ats_alias", updatable = false)
    private String alias;
    @JsonProperty("DocumentTitle")
    @NotBlank
    @Column(name = "ats_titulo_documento")
    private String tituloDocumento;
    @JsonProperty("Severity")
    @Column(name = "ats_gravidade")
    private String gravidade;
    @JsonProperty("InitialReleaseDate")
    @Column(name = "ats_lancamento_inicial")
    private String lancamentoInicial;
    @JsonProperty("CurrentReleaseDate")
    @Column(name = "ats_lancamento_final")
    private String lancamentoFinal;
    @JsonProperty("CvrfUrl")
    @Column(name = "ats_cvrf_url")
    private String cvrfUrl;

}
