package org.seplag.api.model;

public record DadosAtualizacaoDeSeguranca(

        String alias,
        String tituloDocumento,
        String gravidade,
        String lancamentoInicial,
        String lancamentoFinal,
        String cvrfUrl
        ) {
}
