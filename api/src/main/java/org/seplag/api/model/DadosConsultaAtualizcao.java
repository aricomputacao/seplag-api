package org.seplag.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record DadosConsultaAtualizcao(
        @JsonProperty("@odata.context")
        String odataContext,
        @JsonProperty("value")
        List<AtualizacaoDeSeguranca> atualizacoes
        ) {
}
