package org.seplag.api.model;

public record DadosQuantidadeAtualizacaoExercicio(int ano, int quantidade) {
}
