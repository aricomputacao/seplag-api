package org.seplag.api.repository;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AtualizacaoDeSegurancaFilter {
    private String ID;
    private String Alias;
    private String DocumentTitle;
    private String Severity;
    private String InitialReleaseDate;
    private String CurrentReleaseDate;
    private String CvrfUrl;
}
