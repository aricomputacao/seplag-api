package org.seplag.api.repository;

import org.seplag.api.model.AtualizacaoDeSeguranca;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AtualizacaoDeSegurancaRepository  extends JpaRepository<AtualizacaoDeSeguranca, String>, AtualizacaoDeSegurancaRepositoryQuery {
}
