package org.seplag.api.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import org.seplag.api.model.AtualizacaoDeSeguranca;
import org.seplag.api.model.DadosQuantidadeAtualizacaoExercicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class AtualizacaoDeSegurancaRepositoryImpl implements AtualizacaoDeSegurancaRepositoryQuery {


    @Autowired
    private EntityManager manager;

    @Override
    public List<String> consultarIds() {

        TypedQuery<String> tq;
        tq = manager.createQuery(" SELECT a.id FROM  AtualizacaoDeSeguranca a  order by a.lancamentoInicial", String.class);

        return tq.getResultList();
    }

    @Override
    public List<DadosQuantidadeAtualizacaoExercicio> consultarQuantidade() {

        Query q;
        q = manager.createNativeQuery("SELECT SUBSTRING(s.ats_id , 1, 4) as id, count(s.ats_id) from public.atualizacao_seguranca s group by id order by id");

        List<Object[]> list = q.getResultList();

        List<DadosQuantidadeAtualizacaoExercicio> listaRetorno = new ArrayList<>();

        list.forEach( obj -> {
            DadosQuantidadeAtualizacaoExercicio dados = new DadosQuantidadeAtualizacaoExercicio(
                    Integer.parseInt(obj[0].toString()),
                    Integer.parseInt(obj[1].toString()));
            listaRetorno.add(dados);
        });


        return listaRetorno;

    }

    @Override
    public Page<AtualizacaoDeSeguranca> consultar(AtualizacaoDeSegurancaFilter filter, Pageable pageable) {

        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<AtualizacaoDeSeguranca> criteria = builder.createQuery(AtualizacaoDeSeguranca.class);
        Root<AtualizacaoDeSeguranca> root = criteria.from(AtualizacaoDeSeguranca.class);

        //criar restrições
        if (checarSeTemFiltroPreenchido(filter)) {
            Predicate[] predicates = criarRestricoes(filter, builder, root);
            criteria.where(predicates);
        }

        List<Order> orderList = new ArrayList();
        orderList.add(builder.asc(root.get("lancamentoInicial")));
        criteria.orderBy(orderList);

        TypedQuery<AtualizacaoDeSeguranca> query = manager.createQuery(criteria);
        adicionarRestricoesDePaginacao(query, pageable);

        return new PageImpl<>(query.getResultList(), pageable, total(filter));

    }


    private void adicionarRestricoesDePaginacao(TypedQuery<AtualizacaoDeSeguranca> query, Pageable pageable) {
        int paginalAtual = pageable.getPageNumber();
        int totalRegistrosPorPagina = pageable.getPageSize();
        int primeiroRegistroDaPagina = paginalAtual * totalRegistrosPorPagina;

        query.setFirstResult(primeiroRegistroDaPagina);
        query.setMaxResults(totalRegistrosPorPagina);
    }

    private Long total(AtualizacaoDeSegurancaFilter filter) {
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<AtualizacaoDeSeguranca> root = criteria.from(AtualizacaoDeSeguranca.class);

        //criar restrições
        if (checarSeTemFiltroPreenchido(filter)) {
            Predicate[] predicates = criarRestricoes(filter, builder, root);
            criteria.where(predicates);
        }

        criteria.select(builder.count(root));
        TypedQuery<Long> query = manager.createQuery(criteria);

        return query.getSingleResult();

    }

    private Predicate[] criarRestricoes(AtualizacaoDeSegurancaFilter filter, CriteriaBuilder builder, Root<AtualizacaoDeSeguranca> root) {

        List<Predicate> predicates = new ArrayList<>();

        if (filter.getID() != null && !filter.getID().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("id")), "%" + filter.getID().toLowerCase() + "%"));
        }
        if (filter.getAlias() != null && !filter.getAlias().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("alias")), "%" + filter.getAlias().toLowerCase() + "%"));
        }
        if (filter.getDocumentTitle() != null && !filter.getDocumentTitle().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("tituloDocumento")), "%" + filter.getDocumentTitle().toLowerCase() + "%"));
        }
        if (filter.getSeverity() != null && !filter.getSeverity().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("gravidade")), "%" + filter.getSeverity().toLowerCase() + "%"));
        }
        if (filter.getInitialReleaseDate() != null && !filter.getInitialReleaseDate().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("lancamentoInicial")), "%" + filter.getInitialReleaseDate().toLowerCase() + "%"));
        }
        if (filter.getCurrentReleaseDate() != null && !filter.getCurrentReleaseDate().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("lancamentoFinal")), "%" + filter.getCurrentReleaseDate().toLowerCase() + "%"));
        }
        if (filter.getCvrfUrl() != null && !filter.getCvrfUrl().isBlank()) {
            predicates.add(builder.like(builder.lower(root.get("cvrfUrl")), "%" + filter.getCvrfUrl().toLowerCase() + "%"));
        }


        return predicates.toArray(new Predicate[predicates.size()]);

    }

    private boolean checarSeTemFiltroPreenchido(AtualizacaoDeSegurancaFilter filter) {
        if (filter.getID() != null || filter.getDocumentTitle() != null
                || filter.getSeverity() != null || filter.getAlias() != null
                || filter.getCvrfUrl() != null || filter.getCurrentReleaseDate() != null
                || filter.getInitialReleaseDate() != null) {
            return true;
        }
        return false;
    }
}
