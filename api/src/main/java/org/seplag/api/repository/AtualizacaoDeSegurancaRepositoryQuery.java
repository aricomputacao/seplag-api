package org.seplag.api.repository;

import org.seplag.api.model.AtualizacaoDeSeguranca;
import org.seplag.api.model.DadosQuantidadeAtualizacaoExercicio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AtualizacaoDeSegurancaRepositoryQuery  {

    public List<String> consultarIds();
    public List<DadosQuantidadeAtualizacaoExercicio> consultarQuantidade();

    public Page<AtualizacaoDeSeguranca> consultar(AtualizacaoDeSegurancaFilter filter, Pageable pageable);


}
