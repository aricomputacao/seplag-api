package org.seplag.api.resource;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.seplag.api.model.AtualizacaoDeSeguranca;
import org.seplag.api.model.DadosAtualizacaoDeSeguranca;
import org.seplag.api.model.DadosInformacaoIds;
import org.seplag.api.model.DadosQuantidadeAtualizacaoExercicio;
import org.seplag.api.repository.AtualizacaoDeSegurancaFilter;
import org.seplag.api.repository.AtualizacaoDeSegurancaRepository;
import org.seplag.api.service.AtualizacaoDeSegurancaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/atualizacoes")
public class AtualizacaoDeSegurancaResource {

    private AtualizacaoDeSegurancaRepository atualizacaoDeSegurancaRepository;
    private AtualizacaoDeSegurancaService atualizacaoDeSegurancaService;


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable String id) {
        this.atualizacaoDeSegurancaRepository.deleteById(id);
    }


    @GetMapping()
    public Page<AtualizacaoDeSeguranca> consultar(AtualizacaoDeSegurancaFilter atualizacaoDeSegurancaFilter, Pageable pageable) {
        return atualizacaoDeSegurancaRepository.consultar(atualizacaoDeSegurancaFilter, pageable);
    }



    @GetMapping("consultar-ids")
    public List<DadosInformacaoIds> consultarIds() {
        return atualizacaoDeSegurancaRepository.consultarIds()
                .stream()
                .map(DadosInformacaoIds::new).toList();
    }

    @GetMapping("consultar-quantidade-ano")
    public List<DadosQuantidadeAtualizacaoExercicio> consultarQuantidadeAno() {
        return atualizacaoDeSegurancaRepository.consultarQuantidade();
    }
}
