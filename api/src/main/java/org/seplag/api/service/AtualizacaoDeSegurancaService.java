package org.seplag.api.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.seplag.api.model.AtualizacaoDeSeguranca;
import org.seplag.api.model.DadosConsultaAtualizcao;
import org.seplag.api.repository.AtualizacaoDeSegurancaRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class AtualizacaoDeSegurancaService {

    private AtualizacaoDeSegurancaRepository atualizacaoDeSegurancaRepository;

    @Transactional()
    public void salvarAtualizacao() {


        List<String> listaDeIdsRegistrados = atualizacaoDeSegurancaRepository.consultarIds();

        String url = "https://api.msrc.microsoft.com";
        String uri = "/cvrf/v2.0/updates";

        //CONSULTADO DADOS DA API EXTERNA
        DadosConsultaAtualizcao dadosConsultaAtualizcao = WebClient.create(url)
                .get()
                .uri(uri)
                .retrieve()
                .bodyToMono(DadosConsultaAtualizcao.class).block();


        dadosConsultaAtualizcao.atualizacoes().forEach(dados -> {
            //CRIANDO OBJETO COM DOS DADOS DA ATUALIZANDO
            if (!listaDeIdsRegistrados.contains(dados.getId())) {

                AtualizacaoDeSeguranca atualizacaoDeSeguranca = AtualizacaoDeSeguranca.builder()
                        .id(dados.getId())
                        .alias(dados.getAlias())
                        .cvrfUrl(dados.getCvrfUrl())
                        .gravidade(dados.getGravidade())
                        .tituloDocumento(dados.getTituloDocumento())
                        .lancamentoInicial(dados.getLancamentoInicial())
                        .lancamentoFinal(dados.getLancamentoFinal())
                        .build();


                atualizacaoDeSegurancaRepository.save(atualizacaoDeSeguranca);

                log.info("++++++ Adicionado atualização de ID {} e Titulo {}.",
                        atualizacaoDeSeguranca.getId(), atualizacaoDeSeguranca.getTituloDocumento());
            }

        });

    }

    @Scheduled(cron = "0 0/5 * * * *",zone = "America/Fortaleza")
    public void agendamentoInserirAtualizacao() {
            log.info("++++++ Inciando tarefa agendada para inserir atualizações");
            salvarAtualizacao();
            log.info("++++++ Finalizando tarefa agendada para inserir atualizações");
    }


}
