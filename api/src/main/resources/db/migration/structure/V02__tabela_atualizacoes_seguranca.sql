-- Table: public.atualizacao_seguranca

-- DROP TABLE IF EXISTS public.atualizacao_seguranca;

CREATE TABLE IF NOT EXISTS public.atualizacao_seguranca
(
    ats_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    ats_alias character varying(255) COLLATE pg_catalog."default"NOT NULL,
    ats_cvrf_url character varying(255) COLLATE pg_catalog."default",
    ats_gravidade character varying(255) COLLATE pg_catalog."default",
    ats_lancamento_final character varying(255) COLLATE pg_catalog."default" NOT NULL,
    ats_lancamento_inicial character varying(255) COLLATE pg_catalog."default" NOT NULL,
    ats_titulo_documento character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT atualizacao_seguranca_pkey PRIMARY KEY (ats_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.atualizacao_seguranca
    OWNER to postgres;